from flask import Flask, render_template, abort, request

app = Flask(__name__)

@app.route('/')
def index():
    return render_template("index.html")

@app.route('/<name>')
def file(name):
    url = request.path
    print(url)
    if "~" in url or "//" in url or ".." in url:
        abort(403)
    else:
        try:
            return render_template(url)
        except:
            abort(404)

@app.errorhandler(403)
def forbidden(error):
    return render_template("403.html"), 403

@app.errorhandler(404)
def not_found(error):
    if '//' in request.path:
        return render_template("403.html"), 403
    else:
        return render_template("404.html"), 404

if __name__ == "__main__":
    app.run(debug=True,host='0.0.0.0')
