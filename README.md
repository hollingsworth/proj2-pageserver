# README #

A "getting started" manual for Dockers. CIS 322, Introduction to Software Engineering, at the University of Oregon. Reference: https://docs.docker.com/engine/reference/builder/

## Author ##
Sydney Hollingsworth, sholling@uoregon.edu

## Program Function ##
A tiny web server in Python using the framework Flask. Packaged in a Docker Container.
* If the URL ends with `.html` or `.css` (e.g. `path/to/name.html`) , it responds with proper http response. 
* If `name.html` is not in current directory, it responds with 404 (not found). 
* If a page contains one of the symbols(~ // ..), it responds with 403 forbidden error.

## Usage ##

* Build an image called "image name"

  ```
  docker build -t <image name> .
  ```

* Run the image 

  ```
  docker run -d -p 5000:5000 <image name>
  ```

* Stop the image

  ```
  docker stop <image ID>
  ```

* Remove an image

  ```
  docker rmi <image ID>
  ```

